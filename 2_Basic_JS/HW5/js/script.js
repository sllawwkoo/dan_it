/*      Теоретичні питання
1. Опишіть своїми словами, що таке метод об'єкту

Методи об'єкту це функції для роботи з інформацією, що знаходяться в цьому о об'єкті.
Якщо потрібно зробити певні обрахунки чи певні операції в об'єкті тоді застосовують методи об'єкту,
це дуже зручно, практично і читабельно. Наприкладв в завдані ниже треба зробити метод getLogin(), 
який повертатиме першу літеру імені юзера, з'єднану з прізвищем. Ця функція стосується конкретно цього об'єкту, 
тому вона в даному випадку стає методом данного об'єкту. 
_____________________________________________________________________

2. Який тип даних може мати значення властивості об'єкта?

Зазвичай це примітивний тип даних: рядок, число, true, false, null, але може бути і інший об'єкт.
________________________________________________________________________________________________________

3.Об'єкт це посилальний тип даних. Що означає це поняття?

Це означае що коли ми надаємо зсінній значення у вигляді об'єкту, то в оперативній пам"яті комп"ютера
створюється ділянка пам"яті під цей об'єкт, тобто у змінну зберігається лише посиляння на цю ділянку. 
 
*/

/* Завдання */
/* Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без 
використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати ім'я та прізвище.
Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, 
все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). 
Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності
Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. 
Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.*/

function createNewUser() {
	let userName;
	let userLastName;
	while (!userName || !isNaN(userName) || userName === 'null') {
		userName = prompt('Please, enter your name');
	};
	while (!userLastName || !isNaN(userLastName) || userLastName === 'null') {
		userLastName = prompt('Please, enter your last name');
	};
	const newUser = {};
	Object.defineProperty(newUser, 'firstName', {
		value: userName,
		writable: false,
		configurable: true,
	});
	Object.defineProperty(newUser, 'lastName', {
		value: userLastName,
		writable: false,
		configurable: true,
	});
	newUser.getLogin = function () {
		let login = this.firstName[0].toLocaleLowerCase() + this.lastName.toLowerCase()
		return login
	}
	newUser.setFirstName = function (newFirstName) {
		Object.defineProperty(newUser, 'firstName', {
			value: newFirstName,
			writable: false,
			configurable: true,
		});
	};
	newUser.setLastName = function (newLastName) {
		Object.defineProperty(newUser, 'lastName', {
			value: newLastName,
			writable: false,
			configurable: true,
		});
	};
	return newUser
};

let myUser = createNewUser();
console.log(myUser.getLogin());

myUser.firstName = "TEST1";
myUser.lastName = "TEST2";
console.log(myUser);

myUser.setFirstName("TEST1");
myUser.setLastName("TEST2");
console.log(myUser);

