/*      Теоретичні питання
1. Описати своїми словами навіщо потрібні функції у програмуванні.

Функції в програмуванні потрібні для того, щоб зменшити написання коду, тобто
щоб не повторювати код, можна написати один раз функцію і вставляти її там де вона потрібна.
_____________________________________________________________________

2. Описати своїми словами, навіщо у функцію передавати аргумент.

Наприклад у нас є функція function sum (a, b) {return a + b}, a i b параметрами функції, для того 
щоб отримати якийсь результат цієї функції нам потрібно передати в неї аргументи, тобто sum (2, 3), 
результатом буд 5. Передаючи в функцію аргументи які нам потрібні ми можемо отримати різні кінцеві результати.
________________________________________________________________________________________________________

3.Що таке оператор return та як він працює всередині функції?

Оператор return закінчує виконання поточної функції і повертає її значення.
 
*/

/* Завдання */
/* Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має 
бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Отримати за допомогою модального вікна браузера два числа.
Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
Створити функцію, в яку передати два значення та операцію.
Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності
Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав 
не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).*/

let firstNumber = prompt('Enter first number');
let secondNumber = prompt('Enter second nember');

while (isNaN(Number(firstNumber)) === true || isNaN(Number(secondNumber)) === true
	|| firstNumber === null || secondNumber === null || firstNumber.trim() === '' || secondNumber.trim() === '') {
	firstNumber = prompt('Enter first number', firstNumber);
	secondNumber = prompt('Enter second nember', secondNumber);
	if (isNaN(Number(firstNumber)) !== true && isNaN(Number(secondNumber)) !== true
		&& firstNumber !== null && secondNumber !== null && firstNumber.trim() !== '' && secondNumber.trim() !== '') break;
};

let mathOperator;

while (mathOperator !== '+' || mathOperator !== '-' || mathOperator !== '*' || mathOperator !== '/') {
	mathOperator = prompt('Enter the mathematical operation "+", "-", "*" or "/"', mathOperator);
	if (mathOperator === '+' || mathOperator === '-' || mathOperator === '*' || mathOperator === '/') break;
};

function mathOperation(numberOne, numberTwo, operator) {
	switch (operator) {
		case '+':
			let sum = +numberOne + +numberTwo;
			console.log(`${numberOne} + ${numberTwo} = ${sum}`);
			break;
		case '-':
			let subtrac = numberOne - numberTwo;
			console.log(`${numberOne} - ${numberTwo} = ${subtrac}`);
			break;
		case '*':
			let mult = numberOne * numberTwo;
			console.log(`${numberOne} * ${numberTwo} = ${mult}`);
			break;
		case '/':
			let division = numberOne / numberTwo;
			console.log(`${numberOne} : ${numberTwo} = ${division}`);
			break;
		default:
			break;
	}
};

mathOperation(firstNumber, secondNumber, mathOperator)