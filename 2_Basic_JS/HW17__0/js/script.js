
/* Завдання */
/* Створити об'єкт "студент" та проаналізувати його табель. Завдання має бути виконане на чистому Javascript
 без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити порожній об'єкт student, з полями name та lastName.
Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні
про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel.
порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію. */

const student = {
	name: null,
	lastName: null,
}

let studentName;
let studentLastName;
while (!studentName || !isNaN(studentName) || studentName === 'null') {
	studentName = prompt('Введіть ім"я студента.');
};
while (!studentLastName || !isNaN(studentLastName) || studentLastName === 'null') {
	studentLastName = prompt('Введіть прізвище студента');
};

student.name = studentName;
student.lastName = studentLastName;
student.tabel = {};

let course = prompt("Введіть назву предмета");
let courseGrade = +prompt("Введіть оцінку з предмета");

while (course) {
	student.tabel[course] = courseGrade;
	course = prompt("Введіть назву предмета");
	if (course !== null) {
		courseGrade = +prompt("Введіть оцінку з предмета");
	} else break;
}
console.log(student);

let badGrades = false;

for (let key in student.tabel) {
	if (student.tabel[key] < 4) {
		badGrades = true;
		break;
	}
}

if (!badGrades) {
	console.log("Студент переведений на наступний курс");
	let averageGrade = 0;
	let courseCont = 0;

	for (let key in student.tabel) {
		courseCont++;
		averageGrade += student.tabel[key];
	}

	averageGrade = averageGrade / courseCont;

	if (averageGrade > 7) {
		console.log("Студенту призначено стипендію");
	}
}

