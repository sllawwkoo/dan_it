/*      Теоретичні питання
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

Екранування використовується для того щоб вивести спец символ як звичайний за допомогою "\". 
Наприклад якщо у нас буде значення стрінгове "ім"я" то я його вірно буде записати так "ім\"я", тобто екранувати кавички. 
_____________________________________________________________________

2. Які засоби оголошення функцій ви знаєте?
Є три спооби оголошення функцій:  Function Declaration, Function Expression и Named Function Expression.
________________________________________________________________________________________________________

3.Що таке hoisting, як він працює для змінних та функцій?

 Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції піднімаються вгору 
 по своїй області видимості перед виконанням коду. Однією з переваг підйому є те, що він дозволяє нам використовувати 
 функції перед їх оголошенням у коді.
*/

/* Завдання */
/* Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. 
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, 
з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.*/

function createNewUser() {
	let userName;
	let userLastName;
	while (!userName || !isNaN(userName) || userName === 'null') {
		userName = prompt('Please, enter your name');
	};
	while (!userLastName || !isNaN(userLastName) || userLastName === 'null') {
		userLastName = prompt('Please, enter your last name');
	};
	const newUser = {};
	Object.defineProperty(newUser, 'firstName', {
		value: userName,
		writable: false,
		configurable: true,
	});
	Object.defineProperty(newUser, 'lastName', {
		value: userLastName,
		writable: false,
		configurable: true,
	});
	newUser.getLogin = function () {
		let login = this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
		return login
	}
	newUser.setFirstName = function (newFirstName) {
		Object.defineProperty(newUser, 'firstName', {
			value: newFirstName,
			writable: false,
			configurable: true,
		});
	};
	newUser.setLastName = function (newLastName) {
		Object.defineProperty(newUser, 'lastName', {
			value: newLastName,
			writable: false,
			configurable: true,
		});
	};

	let userBirthday = prompt('Enter your date of birth', 'dd.mm.yyyy');
	let arrBirth = userBirthday.split('.');
	let dateBirthday = new Date(arrBirth[2], arrBirth[1] - 1, arrBirth[0]);
	newUser.birthday = dateBirthday;

	newUser.getAge = function () {
		let dateNow = new Date();
		let age = Math.floor((dateNow - this.birthday) / (1000 * 60 * 60 * 24 * 365));
		return age
	}

	newUser.getPassword = function () {
		let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
		return password
	}

	return newUser
};

let myUser = createNewUser();

console.log(myUser);

console.log(myUser.getAge());

console.log(myUser.getPassword());

