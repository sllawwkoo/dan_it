/*      Теоретичні питання
1. Опишіть, як можна створити новий HTML тег на сторінці.

Новий HTML тег на сторінці можна створити за допомогою document.createElement(tag).
_____________________________________________________________________

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

Перши параметр функції insertAdjacentHTML означа куди ми будеморобити вставку по відношенню до елементу HTML.
Існує чотири параметри. 1. beforebegin - вставити html безпосередньо перед елементом. 2. afterbegin - вставити html на початок елементу,
3. beforeend - вставити html в кінець елементую. 4. afterend - вставити html безпосередньо після елементу. 
________________________________________________________________________________________________________

3.Як можна видалити елемент зі сторінки?

Для видалення елемента існує метод node.remove().
*/

/* Завдання */
/* Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. 
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, 
до якого буде прикріплений список (по дефолту має бути document.body.
кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
Приклади масивів, які можна виводити на екран:

["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
["1", "2", "3", "sea", "user", 23];
Можна взяти будь-який інший масив.

Необов'язкове завдання підвищеної складності:
Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, 
виводити його як вкладений список. Приклад такого масиву:

["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.*/

function lists(array, element = document.body) {
	const stringListArray = createLi(array);
	return element.insertAdjacentHTML('beforeend', `<ul>${stringListArray}</ul>`)
};

function createLi(array) {
	const newArray = array.map(item => {
		return Array.isArray(item) ? `<ul>${createLi(item)}</ul>` : `<li>${item}</li>`
	})
	return newArray.join(' ');
}

lists(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);

lists(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"])

const textBeforeTimer = document.createElement('p');
textBeforeTimer.textContent = 'Списки на сторінці зникнуть через:'
document.body.append(textBeforeTimer);
const countTime = document.createElement('span');
countTime.textContent = 3;
countTime.style.cssText = `padding-left: 100px;
									font-size: 25px;
									font-weight: bold;`;
document.body.append(countTime);

let time = +countTime.textContent;
console.log(typeof time)

let timer = setInterval(seconds, 1000)
function seconds() {
	--time
	countTime.textContent = time;
	if (+countTime.textContent < 1) {
		clearInterval(timer)
		document.body.innerHTML = ' '
	}
};

