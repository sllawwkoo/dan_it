/*      Теоретичні питання
1. Які існують типи даних у Javascript?

Object, Symbol, String, Namber, Boolean, BigInt, Undefined, Null.
_____________________________________________________________________

2. У чому різниця між == і ===?

== - порівння і === - суворе порівняння.
Наприклад: 1 == "1" // буде true, тому при такому порівнянні "1" неявно перетворюесться з типу string
на тип number і в результаті 1 дорівнює одному і це правда.
При суворому порівнянні все буде інакше, 1 === "1" буде false, тому що в сувориму порівння спочатку
порівнюються типи операндів, а вданному випадку 1 це number а, "1" це  string,
як би було поріняння 1 === 1, тоді б було true, тому що тип даних number і там і там, а 1 дорівнює 1.
Тобто оператор == порывнює на рівність, а оператор === порівнює на ідентичнысть а також не перетворює
операнди до одного типу. 
________________________________________________________________________________________________________

3.Що таке оператор?

Оператор - це спеціальний символ або вираз для перевірки, зміни чи додавання велечин!  
В JS існують такі оператори, як оператори присвоєння (=), операторі порівняння (<, >, ==? === ),
математичні оператори (+, -, /, *), логічні оператори (||, &&), строкові оператори (наприклад унарний +)
та інші.
*/

/* Завдання */
/* Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою
 модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript
  без використання бібліотек типу jQuery або React.

Технічні вимоги:
Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
Якщо вік менше 18 років - показати на екрані повідомлення: 
You are not allowed to visit this website.
Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: 
Are you sure you want to continue? і кнопками Ok, Cancel. 
Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. 
Якщо користувач натиснув Cancel, показати на екрані повідомлення: 
You are not allowed to visit this website.
Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я,
або при введенні віку вказав не число - запитати ім'я та вік наново
(при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).*/

let answerName = prompt('What is your name?');

while (answerName === "" || answerName === null || answerName === "null" || Number(answerName) === 0) {
	answerName = prompt('What is your name?', answerName);
	if (answerName !== "" && answerName !== null && answerName !== "null" && Number(answerName) !== 0) break;
};

let answerAge = +prompt('How old are you?');

while (isNaN(answerAge) === true || answerAge === 0 || answerAge <= 0) {
	answerAge = +prompt('How old are you?', answerAge);
	if (isNaN(answerAge) !== true && answerAge !== 0 && answerAge > 0) break;
};

if (answerAge < 18) {
	alert('You are not allowed to visit this website.')
} else if (answerAge >= 18 && answerAge <= 22) {
	let answerConfirm = confirm('Are you sure you want to continue?');
	if (answerConfirm === true) {
		alert(`Welcome, ${answerName}`)
	} else {
		alert('You are not allowed to visit this website.')
	}
} else if (answerAge > 22) {
	alert(`Welcome, ${answerName}`)
} 
