
/* Завдання */
/* Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:
У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку 
відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. 
У коментарях зазначено, який текст має відображатися для якої вкладки.
Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть 
додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.*/

const tabsTitle = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-item');

tabsTitle.forEach(item => {
	item.addEventListener('click', () => {
		let tabItemId = item.getAttribute('data-tab');
		let tabItem = document.querySelector(tabItemId);

		if (!item.classList.contains('active')) {
			tabsTitle.forEach(item => {
				item.classList.remove('active');
			});

			tabsItems.forEach(item => {
				item.classList.remove('active');
			});

			item.classList.add('active');
			tabItem.classList.add('active');
		}
	});
});

document.querySelector('.tabs-title').click();
