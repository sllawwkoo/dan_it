
/* Завдання */
/* Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript, 
без использования фреймворков и сторонник библиотек (типа Jquery).
Технические требования:
Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, 
внутренняя вложенность свойств объекта может быть достаточно большой).
Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор. */

let student = {
	name: 'Bill',
	lastName: 'Clinton',
	age: 19,
	table: {
		mathematics: 5,
		chemistry: 4,
		programming: {
			js: 5,
			java: 4,
			php: 4,
		},
		language: [['english', 5], ['german', 4]],
	}
};

function cloneObj(obj) {
	if (typeof (obj) !== 'object') return obj;
	if (!obj) return obj;

	let newObj = (obj instanceof Array) ? [] : {};
	for (let key in obj) {
		if (obj.hasOwnProperty(key)) {
			newObj[key] = cloneObj(obj[key]);
		}
	} return newObj;
}

let newStudent = cloneObj(student);




