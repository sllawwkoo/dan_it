
/* Завдання */
/* Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript 
без використання бібліотек типу jQuery або React.

Технічні вимоги:
У файлі index.html лежить розмітка двох полів вводу пароля.
Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, 
іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.*/

const labels = document.querySelectorAll('.input-wrapper');
const btn = document.querySelector('.btn');
const inputFirst = document.getElementById('input1');
const inputSecond = document.getElementById('input2');
const check = document.querySelector('.check');

labels.forEach(item => {
	const input = item.firstElementChild;
	item.addEventListener('click', (e) => {
		const target = e.target;
		if (target.classList.contains('fa-eye')) {
			target.classList.toggle('hidden');
			target.nextElementSibling.classList.toggle('hidden');
			input.type = 'text'
		} else if (target.classList.contains('fa-eye-slash')) {
			target.classList.toggle('hidden');
			target.previousElementSibling.classList.toggle('hidden');
			input.type = 'password';
		}
	})
});

btn.addEventListener('click', (e) => {
	if (inputFirst.value !== inputSecond.value || inputFirst.value == '' || inputSecond.value == '') {
		check.classList.add('active');
		check.textContent = 'Потрібно ввести однакові значення';
	} else {
		check.classList.remove('active')
		check.textContent = '';
		e.preventDefault();
		setTimeout(() => alert("You are welcome"), 500)
	}
})

