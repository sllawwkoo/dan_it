/* Завдання
Реалізувати можливість зміни колірної теми користувача.Завдання має бути виконане 
на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Взяти будь-яке готове домашнє завдання з HTML/CSS.
Додати на макеті кнопку "Змінити тему".
При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
Вибрана тема повинна зберігатися після перезавантаження сторінки */


const theme = document.querySelector('#theme');
const btn = document.querySelector('.logo-header__btn');
const ramda = document.querySelector('#ramda');
const locStor = localStorage;

btn.addEventListener('click', () => {
	if (theme.getAttribute("href") === "./style/light_style.css") {
		theme.href = './style/dark_style.css';
		btn.textContent = 'Light Theme';
		ramda.src = './img/ramda-white.png';
	} else {
		theme.href = './style/light_style.css';
		btn.textContent = 'Dark Theme';
		ramda.src = './img/ramda.png';
	}
	locStor.setItem('btn', btn.textContent);
});

if (locStor.getItem('btn') === 'Light Theme') {
	theme.href = './style/dark_style.css';
	btn.textContent = 'Light Theme';
	ramda.src = './img/ramda-white.png';
} else {
	theme.href = './style/light_style.css';
	btn.textContent = 'Dark Theme';
	ramda.src = './img/ramda.png';
}


