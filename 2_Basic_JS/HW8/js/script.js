/*      Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)

DOM - це можна так сказати деревоподібна стректура веб сайту у браузер, у вигляді певної кількості об"эктів, вкладених один в одного.
DOM дерево визначає логічний каркас документа, способи доступу до нього та управління ним.
_____________________________________________________________________

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

Якщо ми зробимо такий запис: element.innerText = <p>Some text</p>, то <p>Some text</p> буде браузером прочитаний як текст,
а якщо element.innerHTML = <p>Some text</p>, то втакому випадку ми в element додамо тег <p> з текстом "Some text".
Ось в цьому і полягає різниця.
________________________________________________________________________________________________________

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

До елементів на сторінці можна звернутися за допомогою таких матодів як getElementsByClassName, getElementsByTagNme,
getElementsByName, getElementById - це быльш стары методи але вони , рыдко, але використовуються. Быльш сучасны та 
кращы способи звернення до елемента э querySelector та qureSelectorAll
*/

/* Завдання */
/* Код для завдань лежить в папці project.

Знайти всі параграфи на сторінці та встановити колір фону #ff0000

Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

Встановіть в якості контента елемента з класом testParagraph наступний параграф -

This is a paragraph

Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.*/

let tagsP = document.querySelectorAll('p');

tagsP.forEach(item => {
	item.style.backgroundColor = '#ff0000';
});

let elementOptionsList = document.getElementById('optionsList');
console.log(elementOptionsList);

let parentElem = elementOptionsList.parentElement;
console.log(parentElem);

let childrenOptionList = elementOptionsList.children;

Array.from(childrenOptionList).forEach(item => {
	console.log(` Назва ноди - ${item.nodeName}\n Тип ноди - ${item.nodeType}`)
})

let testParagraph = document.getElementById('testParagraph');
testParagraph.textContent = 'This is a paragraph';

console.log(testParagraph);

let mainHeader = document.querySelector('.main-header');
let childrenMainHeader = mainHeader.children;
Array.from(childrenMainHeader).forEach(item => {
	item.classList.add('nav-item');
	console.log(item)
});

let sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(item => {
	item.classList.remove('section-title');
	console.log(item)
});









