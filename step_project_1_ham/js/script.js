/*-----Buttons-----*/
const buttons = document.querySelectorAll('.button');

buttons.forEach(item => {
	item.addEventListener('mousedown', () => {
		item.classList.add('active')
	})
});
buttons.forEach(item => {
	item.addEventListener('mouseup', () => {
		item.classList.remove('active')
	})
})
/*---- Services -----*/

const tabsTitle = document.querySelectorAll('.menu-services__item');
const tabsText = document.querySelectorAll('.content-services__column');

tabsTitle.forEach(item => {
	item.addEventListener('click', () => {
		let tabItemId = item.getAttribute('data-tab');
		let tabItem = document.querySelector(tabItemId);

		if (!item.classList.contains('active__menu-services')) {
			tabsTitle.forEach(item => {
				item.classList.remove('active__menu-services');
			});

			tabsText.forEach(item => {
				item.classList.remove('active-content');
			});

			item.classList.add('active__menu-services');
			tabItem.classList.add('active-content');
		}
	});
});

document.querySelector('.menu-services__item').click();

/*----Work----*/

const workImg = document.querySelector('.work__gallery');
const graphicDesign = 'Graphic Design';
const webDesign = 'Web Design';
const landingPages = 'Landing Pages';
const wordpress = 'Wordpress';
const btnLoadWork = document.querySelector('.work__button');
const loader = document.querySelector('.work__loader');

function createImgTemplate(groupImg, indexImg) {
	return `	<div class="work__img active-img ibg" data-category="${groupImg}">
	<img src="./img/work/${groupImg}/${groupImg}${indexImg}.jpg" alt="${groupImg}">
	<div class="work__reverse-card card-reverse">
		<div class="card-reverse__icon icon">
			<a href="#" class="icon__link">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="43" height="43"
					viewBox="0 0 1048 1024">
					<title></title>
					<g id="icomoon-ignore">
					</g>
					<path fill="none" id="none1" stroke="#18cfab" stroke-linejoin="miter"
						stroke-linecap="butt" stroke-miterlimit="4" stroke-width="23.814"
						d="M525.884 216.358h15.161c167.472 0 303.223 135.752 303.223 303.223s-135.752 303.223-303.223 303.223h-15.161c-167.472 0-303.223-135.752-303.223-303.223s135.752-303.223 303.223-303.223z">
					</path>
					<path fill="#18cfab" id="green1"
						d="M615.538 454.821l-27.574-27.844c-12.194-12.289-32.011-12.366-44.245-0.133l-30.591 30.474c-12.245 12.224-12.299 32.108-0.133 44.399l17.486-17.151c-1.763-5.473 0.402-11.731 4.736-16.058l19.463-19.37c6.159-6.158 16.052-6.109 22.135 0.036l16.547 16.734c6.083 6.136 6.059 16.052-0.059 22.216l-19.463 19.387c-4.112 4.108-10.874 6.352-16.124 4.908l-17.116 17.173c12.186 12.328 31.976 12.375 44.245 0.139l30.551-30.455c12.285-12.224 12.326-32.128 0.133-44.441zM503.546 566.449c1.459 5.34-0.872 12.253-5.083 16.469l-18.174 18.156c-6.235 6.2-16.317 6.192-22.529-0.095l-16.886-16.999c-6.197-6.257-6.158-16.432 0.081-22.636l18.189-18.144c4.429-4.416 10.801-6.632 16.355-4.757l17.53-17.773c-12.413-12.515-32.615-12.57-45.075-0.133l-29.498 29.432c-12.479 12.467-12.53 32.729-0.133 45.241l28.067 28.406c12.402 12.477 32.578 12.57 45.075 0.133l29.498-29.432c12.459-12.489 12.527-32.697 0.133-45.241l-17.573 17.397zM499.473 558.445c-4.215 4.226-10.973 4.219-15.104-0.077-4.15-4.302-4.112-11.187 0.059-15.444l50.401-51.226c4.188-4.226 10.935-4.219 15.070 0.048 4.179 4.264 4.15 11.166-0.057 15.445l-50.392 51.247z">
					</path>
				</svg>
			</a>
			<a href="#" class="icon__link">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="43" height="43"
					viewBox="0 0 1048 1024">
					<title></title>
					<g id="icomoon-ignore">
					</g>
					<path fill="none" id="none2" stroke="#18cfab" stroke-linejoin="miter"
						stroke-linecap="butt" stroke-miterlimit="4" stroke-width="23.814"
						d="M517.364 231.958h14.361c158.637 0 287.223 128.587 287.223 287.223s-128.587 287.223-287.223 287.223h-14.361c-158.637 0-287.223-128.587-287.223-287.223s128.587-287.223 287.223-287.223z">
					</path>
					<path fill="#18cfab" id="green2"
						d="M440.621 447.376h172.334v157.973h-172.334v-157.973z"></path>
				</svg>
			</a>
		</div>
		<h2 class="card-reverse__title">creative design</h2>
		<h3 class="card-reverse__subtitle">${groupImg}</h3>
	</div>
</div>`
};

for (let i = 1; i < 4; i++) {
	let graph = createImgTemplate(graphicDesign, i);
	let web = createImgTemplate(webDesign, i);
	let land = createImgTemplate(landingPages, i);
	let word = createImgTemplate(wordpress, i);

	workImg.insertAdjacentHTML("beforeend", graph);
	workImg.insertAdjacentHTML("beforeend", web);
	workImg.insertAdjacentHTML("beforeend", land);
	workImg.insertAdjacentHTML("beforeend", word);
}

btnLoadWork.addEventListener('click', () => {
	if (document.querySelectorAll('.work__img').length === 12) {
		btnLoadWork.classList.add('disable');
		loader.classList.add('active');
		setTimeout(() => {
			loader.classList.remove('active');
			for (let i = 4; i < 7; i++) {
				let graph = createImgTemplate(graphicDesign, i);
				let web = createImgTemplate(webDesign, i);
				let land = createImgTemplate(landingPages, i);
				let word = createImgTemplate(wordpress, i);

				workImg.insertAdjacentHTML("beforeend", graph);
				workImg.insertAdjacentHTML("beforeend", web);
				workImg.insertAdjacentHTML("beforeend", land);
				workImg.insertAdjacentHTML("beforeend", word);
			}
			btnLoadWork.classList.remove('disable');
		}, 5000);
	};
	if (document.querySelectorAll('.work__img').length === 24) {
		btnLoadWork.classList.add('disable');
		loader.classList.add('active');
		setTimeout(() => {
			loader.classList.remove('active');
			for (let i = 7; i < 13; i++) {
				if (i === 7) {
					let graph = createImgTemplate(graphicDesign, i);
					let web = createImgTemplate(webDesign, i);
					let land = createImgTemplate(landingPages, i);
					let word = createImgTemplate(wordpress, i);

					workImg.insertAdjacentHTML("beforeend", graph);
					workImg.insertAdjacentHTML("beforeend", web);
					workImg.insertAdjacentHTML("beforeend", land);
					workImg.insertAdjacentHTML("beforeend", word);
				} else if (i > 7 && i < 11) {
					let graph = createImgTemplate(graphicDesign, i);
					let word = createImgTemplate(wordpress, i);

					workImg.insertAdjacentHTML("beforeend", graph);
					workImg.insertAdjacentHTML("beforeend", word);
				} else {
					let graph = createImgTemplate(graphicDesign, i);
					workImg.insertAdjacentHTML("beforeend", graph);
				}
			}
		}, 5000);
	}
});

const workTitle = document.querySelectorAll('.work__item');

workTitle.forEach(item => {
	item.addEventListener('click', () => {
		let title = item.dataset.name;
		workTitle.forEach(element => {
			element.classList.remove('active')
		});
		item.classList.add('active')
		let workCard = document.querySelectorAll('.work__img');
		workCard.forEach(element => {
			element.classList.add('active-img')
		});
		workCard.forEach(card => {
			if (card.dataset.category !== title) {
				card.classList.remove('active-img')
			};
			if (title === 'All') {
				card.classList.add('active-img')
			}
		})
	})
});

document.querySelector('.work__item').click();

/*-----People-----*/

const btnFaces = document.querySelectorAll('.faces__btn');
const facesItem = document.querySelectorAll('.faces__item');
const sliderItem = document.querySelectorAll('.slider__column');
let count = 1;

btnFaces.forEach(item => {
	item.addEventListener('click', (e) => {
		if (e.target.dataset.name === 'next') {
			count++;
			if (count > facesItem.length) {
				count = 1
			};
			chooseSliderBlock('next')
		};
		if (e.target.dataset.name === 'prev') {
			count--;
			if (count < 1) {
				count = facesItem.length
			};
			chooseSliderBlock('prev')
		}
	})
});

facesItem.forEach(item => {
	item.addEventListener('click', (e) => {
		const target = e.target
		count = Number(target.dataset.id)
		chooseSliderBlock()
	})
});

function chooseSliderBlock(buttonName) {
	facesItem.forEach(face => {
		face.classList.remove('active')
		if (face.dataset.id === count.toString()) {
			face.classList.add('active')
		}
	})
	if (sliderItem) {
		sliderItem.forEach(item => {
			item.classList.remove('active')
			if (item.dataset.id === count.toString()) {
				item.classList.add('active')
				buttonName === 'next' ? item.style.animation = 'scale-display-left .7s ease-in-out' : item.style.animation = 'scale-display-right .7s ease-in-out'
			}
		})
	}
}

