import "./style.scss";
import { Button } from "../button";
import { Modal } from "../modal";
import { HeartNonSolid, HeartSolid, Basket, Cross } from "../icons";

export function ProductCard(props) {
	const { card, toggleModal, addToFavorite, isFavs, isModal, isCart, isBasket, addToBasket, removeFromBasket } = props;

	return (
		<>
			<div className="card">
				<div className="card__container">
					{isCart ? <Button text={<Cross />} className={'card__btn-cross'} onClick={toggleModal} /> : null}
					<div className="card__img">
						<img src={card.url} alt={card.name} />
					</div>
					<div className="card__name">{card.name}</div>
					<div className="card__article">Артикул: <span>{card.article}</span></div>
					<div className="card__color">Колір: <span>{card.color}</span></div>
					<div className="card__price">Ціна: <span>{card.price} ₴</span></div>
					<div className="card__btn-and-fave">
						<>
							<Button text='Додати до кошика' className={'card__btn-basket'} onClick={!isBasket ? toggleModal : null} />
							{isBasket ? <div className="card__basket"><Basket /></div> : null}
							<Button text={
								isFavs ? <HeartSolid fill='red' width={35} height={35} />
									: <HeartNonSolid fill='red' width={35} height={35} />}
								className={'card__btn-fave'}
								onClick={addToFavorite} />
						</>
					</div>
				</div>
			</div>
			{isModal ? <Modal
				closeButton={true}
				onCloseModal={toggleModal}
				header={!isCart ? "Ви дійсно хочете додати до кошика?"
					: "Ви дійсно хочете видалити з кошика?"}
				name={card.name}
				article={card.article}
				color={card.color}
				price={card.price}
				actions={
					<>
						<Button
							className={'btn-ok'}
							text='Ok'
							onClick={!isCart ? (
								() => {
									addToBasket();
									toggleModal();
								}
							) : (
								() => {
									removeFromBasket();
									toggleModal();
								}
							)
							}
						/>
						<Button
							className={'btn-cancel'}
							text='Cancel'
							onClick={toggleModal}
						/>
					</>
				} /> : null
			}
		</>
	)
}