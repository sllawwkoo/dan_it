import "./style.scss";
import { ProductCard } from "../product-card";

export function ProductList(props) {

	const { data, toggleModal, addToBasket, addToFavorite, favsCards,
		isModal, basketCards, selectedCard, isFavorite, isCart, removeFromBasket, } = props;

	const cardsToRender = isCart ? basketCards : isFavorite ? favsCards : data;

	return (
		<>
			{cardsToRender.map((card) => (<ProductCard
				key={(card.article)}
				card={card}
				toggleModal={() => toggleModal(card.article)}
				addToBasket={() => addToBasket(card)}
				removeFromBasket={() => removeFromBasket(card)}
				addToFavorite={() => addToFavorite(card)}
				isModal={isModal && selectedCard === card.article}
				isFavs={favsCards && favsCards.some(item => item.article === card.article)}
				isBasket={basketCards.some(item => item.article === card.article)}
				favsCards={favsCards}
				basketCards={basketCards}
				isCart={isCart}
				isFavorite={isFavorite}
			/>))}
		</>
	);
}