import { ProductList } from "../components/product-list";

export function Cart(props) {

	const { addToFavorite, basketCards, removeFromBasket, toggleModal, isModal, selectedCard, favsCards } = props;

	return (
		<>
			<h1 style={{ textAlign: 'center' }}>Кошик</h1>
			<div className="cards__body">
				{basketCards.length === 0 ? (
					<p>Ваш кошик порожній</p>
				) : (
					<ProductList
						addToFavorite={addToFavorite}
						removeFromBasket={removeFromBasket}
						isCart={true}
						basketCards={basketCards}
						toggleModal={toggleModal}
						isModal={isModal}
						selectedCard={selectedCard}
						favsCards={favsCards}
					/>
				)
				}
			</div>
		</>
	);
}