import { basketCardsTypes } from "../types";

export function setBasketCards(basketCards) {
	return {
		type: basketCardsTypes.SET_BASKET_CARDS,
		payload: basketCards
	}
}

export function updateBasketCards(basketCards) {
	return function (dispatch) {
		localStorage.setItem("cardsBasket", JSON.stringify(basketCards));
		dispatch(setBasketCards(basketCards))
	};
}

export function sellBasketCards(basketCards) {
	return function (dispatch) {
		localStorage.setItem("cardsBasket", JSON.stringify(basketCards));
		dispatch(setBasketCards(basketCards))
	};
}