import { isModalTypes } from "../types";

const initislState = {
	isModal: false
}

export function isModalReducer(state = initislState, action) {
	switch (action.type) {
		case isModalTypes.IS_MODAL:
			return {
				...state,
				isModal: !state.isModal
			}
		default:
			return state;
	}
}