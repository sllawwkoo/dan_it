import { updateFavsCards, setFavsCards } from "../actions/favsCards";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

describe("updateFavsCards", () => {
	test("should dispatch setFavsCards action and save to localStorage", () => {
	  const favsCards = [/* your test data */];
	  const dispatchMock = jest.fn();
	  const setItemMock = jest.fn(); // замість spy
	  Object.defineProperty(window, "localStorage", { value: { setItem: setItemMock } });
 
	  updateFavsCards(favsCards)(dispatchMock);
 
	  expect(setItemMock).toHaveBeenCalledWith(
		 "cardsFave",
		 JSON.stringify(favsCards)
	  );
	  expect(dispatchMock).toHaveBeenCalledWith(setFavsCards(favsCards));
	});
 });