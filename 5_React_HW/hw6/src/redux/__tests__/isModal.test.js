import { isModal } from "../actions/isModal";
import { isModalTypes } from "../types";
import { cleanup } from "@testing-library/react";

afterEach(cleanup);

describe("isModal", () => {
  test("should return the correct action object", () => {
    const expectedAction = {
      type: isModalTypes.IS_MODAL,
    };

    const action = isModal();

    expect(action).toEqual(expectedAction);
  });
});