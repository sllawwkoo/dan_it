import { cleanup } from "@testing-library/react";
import localStorageMock from "jest-localstorage-mock";
import { setBasketCards, updateBasketCards, sellBasketCards } from "../actions/basketCards"; 
import { basketCardsTypes } from "../types";

afterEach(cleanup);

test("setBasketCards returns the correct action", () => {
  const basketCards = [{ id: 1, name: "Product 1" }];
  const expectedAction = {
    type: basketCardsTypes.SET_BASKET_CARDS,
    payload: basketCards,
  };
  const action = setBasketCards(basketCards);
  expect(action).toEqual(expectedAction);
});

beforeAll(() => {
	// Мокуємо localStorage для тестів
	global.localStorage = localStorageMock;
 });
 
 test("updateBasketCards dispatches the correct actions and saves to localStorage", () => {
	const basketCards = [{ id: 1, name: "Product 1" }];
 
	const dispatchMock = jest.fn();
 
	updateBasketCards(basketCards)(dispatchMock);
 
	expect(localStorage.setItem).toHaveBeenCalledWith(
	  "cardsBasket",
	  JSON.stringify(basketCards)
	);
 
	expect(dispatchMock).toHaveBeenCalledWith(
	  setBasketCards(basketCards)
	);
 });

 test("sellBasketCards dispatches the correct actions and saves to localStorage", () => {
	const basketCards = [{ id: 1, name: "Product 1" }];
 
	const dispatchMock = jest.fn();
	const setItemMock = jest.spyOn(localStorage, "setItem");
 
	sellBasketCards(basketCards)(dispatchMock);
 
	expect(setItemMock).toHaveBeenCalledWith(
	  "cardsBasket",
	  JSON.stringify(basketCards)
	);
 
	expect(dispatchMock).toHaveBeenCalledWith(
	  setBasketCards(basketCards)
	);
 });