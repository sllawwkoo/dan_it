import './App.scss';
import { Routes, Route } from 'react-router-dom';
import { Home, Cart, Favs } from "./pages";
import { Header } from './components/header';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { isModal } from './redux/actions/isModal';
import { updateBasketCards } from './redux/actions/basketCards';
import { updateFavsCards } from './redux/actions/favsCards';
import { ViewModeContext } from './context/viewModeContext';



function App() {

	const dispatch = useDispatch();
	const { basketCards } = useSelector((state) => state.basketCards);
	const { favsCards } = useSelector((state) => state.favsCards);

	const [selectedCard, setSelectedCard] = useState(null);
	const [viewMode, setViewMode] = useState(false);

	const addToBasket = (card) => {
		const index = basketCards.findIndex(item => item.article === card.article);

		if (index === -1) {
			const newCardsBasket = [...basketCards, card];
			dispatch(updateBasketCards(newCardsBasket));
		}
	};

	const removeFromBasket = (card) => {
		const newCardsBasket = basketCards.filter(item => item.article !== card.article);
		dispatch(updateBasketCards(newCardsBasket))
	};

	const addToFavorite = (card) => {
		if (card && card.article) {
			const index = favsCards.findIndex(item => item.article === card.article);

			if (index !== -1) {
				const newFavsCards = favsCards.filter(item => item.article !== card.article);
				dispatch(updateFavsCards(newFavsCards));
			} else {
				const newFavsCards = [...favsCards, card];
				dispatch(updateFavsCards(newFavsCards));
			}
		}
	};

	const toggleModal = (article) => {
		if (selectedCard === article) {
			setSelectedCard(null);
			dispatch(isModal())
		} else {
			setSelectedCard(article);
			dispatch(isModal())
		}
	};

	const toggleViewMode = () => {
		setViewMode(!viewMode)
	}

	return (
		<ViewModeContext.Provider value={{viewMode, setViewMode, toggleViewMode}}>
			<div className='wrapper'>
				<Header/>
				<main className='page'>
					<section className='page__cards cards'>
						<div className="cards__container container">
							<Routes>
								<Route path='/' element={<Home
									toggleModal={toggleModal}
									addToBasket={addToBasket}
									addToFavorite={addToFavorite}
									selectedCard={selectedCard}
								/>} />
								<Route path='/cart' element={<Cart
									removeFromBasket={removeFromBasket}
									addToFavorite={addToFavorite}
									toggleModal={toggleModal}
									selectedCard={selectedCard}
								/>} />
								<Route path='/favs' element={<Favs
									toggleModal={toggleModal}
									addToBasket={addToBasket}
									addToFavorite={addToFavorite}
									selectedCard={selectedCard}
								/>} />
							</Routes>
						</div>
					</section>
				</main>
			</div>
		</ViewModeContext.Provider>
	);
}

export default App
