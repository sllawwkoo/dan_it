import { Cross } from "../icons";
import "./style.scss";

export function Modal(props) {

	const { header, closeButton, actions, onCloseModal, name, article, color, price } = props;

	return (
		<div className="modal" onClick={onCloseModal} data-testid="modal">
			<div className="modal__content" onClick={e => e.stopPropagation()} data-testid="modal-content">
				<h4 className="modal__header">{header}</h4>
				{closeButton ?
					<button className="modal__xbtn" onClick={onCloseModal}>
						<Cross />
					</button>
					: null}
				<div className="modal__name">{name}</div>
				<div className="modal__text">Артикул: {article}</div>
				<div className="modal__text">Колір: {color}</div>
				<div className="modal__text">Ціна: {price} ₴</div>
				<div className="modal__btn">
					{actions}
				</div>
			</div>
		</div>
	)
}