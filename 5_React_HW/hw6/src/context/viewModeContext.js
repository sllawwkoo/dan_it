import { createContext } from "react";

export const ViewModeContext = createContext({
	viewMode: false,
	setViewMode: () => {},
	toggleViewMode: () => {}
})