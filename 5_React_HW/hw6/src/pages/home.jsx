import { useDispatch, useSelector } from "react-redux";
import { ProductList } from "../components/product-list";
import { useContext, useEffect } from "react";
import { getCardsAsync } from "../redux/actions/cards";
import { ViewModeContext } from "../context/viewModeContext";

export function Home(props) {

	const { toggleModal, addToBasket, addToFavorite, selectedCard} = props

	const dispatch = useDispatch();
	const { loading, data, error, isLoaded } = useSelector((state) => state.cards)
	useEffect(() => {
		if (!isLoaded) dispatch(getCardsAsync())
	}, [dispatch, isLoaded]);

	const {viewMode} = useContext(ViewModeContext);

	const errorMessage = error ? <h2>An error has occurred</h2> : null;
	return (
		<>
			<h1 className="cards__title" >Мобільні тлефони</h1>
			<div className={!viewMode ? "cards__body" : "home-cards__body"}>
				{loading ? (
					<div>loading data ...</div>
				) : (
					<>
						{errorMessage}
						<ProductList
							data={data}
							toggleModal={toggleModal}
							addToBasket={addToBasket}
							addToFavorite={addToFavorite}
							selectedCard={selectedCard}						
						/>
					</>
				)}
			</div>
		</>
	)
}