export const modalsData = [
	{
	  id: 1,
	  text: 'Open first modal',
	  backgroundColor: '#91e791',
	  className: 'btn__primary first-btn',
	  dataId: 1,
	  modal: {
		 closeButton: true,
		 header: 'Do you want to delete this file?',
		 text: "Once you delete this file? it won't be possible to undo this action. Are you sure you want to delete it?",
		 actions: [
			{
			  text: 'Ok',
			  className: 'btn-ok'
			},
			{
			  text: 'Cancel',
			  className: 'btn-cancel'
			}
		 ]
	  }
	},
	{
	  id: 2,
	  text: 'Open second modal',
	  backgroundColor: '#e2adda',
	  className: 'btn__primary second-btn',
	  dataId: 2,
	  modal: {
		 closeButton: true,
		 header: 'Do you want to continue working?',
		 text: 'If so, a new account will be created! Continue work?',
		 actions: [
			{
			  text: 'Ok',
			  className: 'btn-ok'
			},
			{
			  text: 'Cancel',
			  className: 'btn-cancel'
			}
		 ]
	  }
	}
 ];