import { Component } from 'react';
import Button from './components/Button';
import Modal from './components/Modal';
import { modalsData } from './modalsData';
import './App.scss';

class App extends Component {
	constructor(props) {
		super(props)

		this.state = {
			activeModal: false,
			modalState: {
				modalId: '',
				modalCloseButton: '',
				modalHeader: '',
				modalText: '',
				modalActions: [],
			}
		}
	}

	toggleModal = (e) => {
		const modalId = e.target.dataset.modalId;
		const modal = modalsData.find((modal) => modal.id === +modalId);
		this.setState({
		  activeModal: !this.state.activeModal,
		  modalState: this.state.activeModal ? {
			 modalId: '',
			 modalCloseButton: '',
			 modalHeader: '',
			 modalText: '',
			 modalActions: '',
		  } : {
			 modalId: modal.id,
			 modalCloseButton: modal.modal.closeButton,
			 modalHeader: modal.modal.header,
			 modalText: modal.modal.text,
			 modalActions: modal.modal.actions,
		  },
		});
	 };

	render() {

		return (
			<>
				<div className='btn__wrapper'>
					{modalsData.map((modal) => (
						<Button
							key={modal.id}
							className={modal.className}
							text={modal.text}
							backgroundColor={modal.backgroundColor}
							onClick={this.toggleModal}
							dataModalId={modal.dataId}
						/>
					))}
				</div>

				{this.state.activeModal ?
					<Modal
						key={this.state.modalState.modalId}
						closeButton={this.state.modalState.modalCloseButton}
						onClick={this.toggleModal}
						header={this.state.modalState.modalHeader}
						text={this.state.modalState.modalText}
						actions={this.state.modalState.modalActions.map((action) => (
							<Button
								key={action.text}
								className={action.className}
								text={action.text}
								onClick={this.toggleModal}
							/>
						))}
					/> : null
				}
			</>
		);
	}
}

export default App;
