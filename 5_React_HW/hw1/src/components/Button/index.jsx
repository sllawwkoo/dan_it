import { Component } from "react";
import "./style.scss"

class Button extends Component {

	render() {
		const { backgroundColor, text, className, onClick, dataModalId } = this.props;
		const btnBackground = { backgroundColor: backgroundColor	};		

		return (
			<button className={className} data-modal-id={dataModalId} style={btnBackground} onClick={onClick}>
				{text}
			</button>
		)
	}
}

export default Button