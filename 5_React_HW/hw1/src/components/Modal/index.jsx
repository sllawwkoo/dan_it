import { Component } from "react";
import "./style.scss"

class Modal extends Component {

	render() {
		const { header, closeButton, text, actions, onClick } = this.props;

		return (
			<div className="modal" onClick={onClick}>
				<div className="modal__content" onClick={e => e.stopPropagation()}>
					<h4 className="modal__header">{header}</h4>
					{closeButton ?
						<button className="modal__xbtn" onClick={onClick}>
							<svg width="26" height="26" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M4.11 2.697L2.698 4.11 6.586 8l-3.89 3.89 1.415 1.413L8 9.414l3.89 3.89 1.413-1.415L9.414 8l3.89-3.89-1.415-1.413L8 6.586l-3.89-3.89z" fill="#fff"></path>
							</svg>
						</button>
						: null}
					<div className="modal__text">{text}</div>
					<div className="modal__btn">
						{actions}
					</div>
				</div>
			</div>
		)
	}
}

export default Modal