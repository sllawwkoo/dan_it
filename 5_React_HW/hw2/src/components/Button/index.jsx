import { Component } from "react";
import PropTypes from "prop-types";
import "./style.scss"

class Button extends Component {

	render() {
		const { backgroundColor, text, className, onClick } = this.props;
		const btnBackground = { backgroundColor: backgroundColor	};		

		return (
			<button className={className} style={btnBackground} onClick={onClick}>
				{text}
			</button>
		)
	}
}

Button.propTypes = {
	backgroundColor: PropTypes.string,
	text: PropTypes.node.isRequired,
	className: PropTypes.string,
	onClick: PropTypes.func,
};

export default Button