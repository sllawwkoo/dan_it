import { Component } from "react";
import HeartNotSolid from "../HeartNotSolid";
import Basket from "../Basket";
import PropTypes from 'prop-types';
import './style.scss'

class Header extends Component {	
	render() {
		const { countBasket, countFave } = this.props
		return (
			<header className="header">
				<div className="header__container container">
					<div className="header__logo">
						<img src="img/POCO_logo.png" alt="Poco" />
					</div>
					<div className="header__nav">
						<div className="header__fave">
							<HeartNotSolid width={35} height={35} />
							<span className="span-fave"
								style={{
									backgroundColor: +countFave !== 0 ? '#e31837' : null,
									padding: +countFave !== 0 ? '4px 8px' : null
								}}>
								{countFave !== 0 ? countFave : ''}
							</span>
						</div>
						<div className="header__basket">
							<Basket width={35} height={35} />
							<span className="span-cart"
								style={{
									backgroundColor: +countBasket !== 0 ? '#e31837' : null,
									padding: +countBasket !== 0 ? '4px 8px' : null
								}}>
								{countBasket !== 0 ? countBasket : ''}
							</span>
						</div>
					</div>
				</div>
			</header>
		)
	}
}

Header.propTypes = {
	countBasket: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
	countFave: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
 };

export default Header
