import { Component } from "react";
import PropTypes from 'prop-types';

class HeartSolid extends Component {
	render() {
		const width = this.props.width || 30;
		const height = this.props.height || 30;
		const color = this.props.color || "black";
		return (
			<svg
				xmlns="http://www.w3.org/2000/svg"
				width={width}
				height={height}
				viewBox="0 0 16 16"
				fill={color}
			>
				<path
					fillRule="evenodd"
					d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
				/>
			</svg>
		)
	}
}

HeartSolid.propTypes = {
	width: PropTypes.number,
	height: PropTypes.number,
	color: PropTypes.string,
};

HeartSolid.defaultProps = {
	height: 30,
	width: 30,
	color: 'black',
};

export default HeartSolid