import { Component } from "react";
import PropTypes from "prop-types";
import "./style.scss"

class Modal extends Component {

	render() {
		const { header, closeButton, actions, onCloseModal, name, article, color, price } = this.props;

		return (
			<div className="modal" onClick={onCloseModal}>
				<div className="modal__content" onClick={e => e.stopPropagation()}>
					<h4 className="modal__header">{header}</h4>
					{closeButton ?
						<button className="modal__xbtn" onClick={onCloseModal}>
							<svg width="26" height="26" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
								<path fillRule="evenodd" clipRule="evenodd" d="M4.11 2.697L2.698 4.11 6.586 8l-3.89 3.89 1.415 1.413L8 9.414l3.89 3.89 1.413-1.415L9.414 8l3.89-3.89-1.415-1.413L8 6.586l-3.89-3.89z" fill="#010101"></path>
							</svg>
						</button>
						: null}
					<div className="modal__name">{name}</div>
					<div className="modal__text">Артикул: {article}</div>
					<div className="modal__text">Колір: {color}</div>
					<div className="modal__text">Ціна: {price} ₴</div>
					<div className="modal__btn">
						{actions}
					</div>
				</div>
			</div>
		)
	}
}

Modal.propTypes = {
	header: PropTypes.string.isRequired,
	closeButton: PropTypes.bool.isRequired,
	actions: PropTypes.node.isRequired,
	onCloseModal: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
	article: PropTypes.string.isRequired,
	color: PropTypes.string.isRequired,
	price: PropTypes.number.isRequired,
 };

export default Modal