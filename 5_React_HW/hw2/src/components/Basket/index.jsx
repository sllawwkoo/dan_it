import { Component } from "react";
import PropTypes from "prop-types";

class Basket extends Component {
	render() {
		const height = this.props.height || 25;
		const width = this.props.width || 25;
		const color = this.props.color || 'black';
		return (
			<svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} fill={color} viewBox="0 0 16 16">
			<path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
		 </svg>
		);
	 }
}

Basket.propTypes = {
	height: PropTypes.number,
	width: PropTypes.number,
	color: PropTypes.string,
 };
 
 Basket.defaultProps = {
	height: 25,
	width: 25,
	color: 'black',
 };
 
 export default Basket;
 
 
 
 
 
 
 
 