import { Component } from "react";
import Button from "../Button";
import HeartNotSolid from "../HeartNotSolid";
import HeartSolid from "../HeartSolid";
import Basket from "../Basket";
import Modal from "../Modal";
import PropTypes from "prop-types";
import "./style.scss";

class Card extends Component {

	render() {
		const { card, toggleModal, isFavorite, isModal, addToBasket, addToFavorite, isBasket } = this.props;
		return (
			<>
				<div className="card">
					<div className="card__container">
						<div className="card__img">
							<img src={card.url} alt={card.name} />
						</div>
						<div className="card__name">{card.name}</div>
						<div className="card__article">Артикул: <span>{card.article}</span></div>
						<div className="card__color">Колір: <span>{card.color}</span></div>
						<div className="card__price">Ціна: <span>{card.price} ₴</span></div>
						<div className="card__btn-and-fave">
							<Button text='Додати до кошика' className={'card__btn-basket'} onClick={!isBasket ? toggleModal : null} />
							{isBasket ? <div className="card__basket"><Basket /></div> : null}
							<Button text={
								!isFavorite ? <HeartNotSolid color='red' width={35} height={35} />
									: <HeartSolid color='red' width={35} height={35} />}
								className={'card__btn-fave'}
								onClick={addToFavorite} />
						</div>
					</div>
				</div>
				{isModal ? <Modal
					closeButton={true}
					onCloseModal={toggleModal}
					header="Ви дійсно хочете додати до кошика?"
					name={card.name}
					article={card.article}
					color={card.color}
					price={card.price}
					actions={
						<>
							<Button
								className={'btn-ok'}
								text='Ok'
								onClick={() => {
									addToBasket();
									toggleModal();
								}}
							/>
							<Button
								className={'btn-cancel'}
								text='Cancel'
								onClick={toggleModal}
							/>
						</>
					} /> : null
				}
			</>
		)
	}
}

Card.propTypes = {
	card: PropTypes.object.isRequired,
	toggleModal: PropTypes.func.isRequired,
	isFavorite: PropTypes.bool.isRequired,
	isModal: PropTypes.bool.isRequired,
	addToBasket: PropTypes.func.isRequired,
	addToFavorite: PropTypes.func.isRequired,
	isBasket: PropTypes.bool.isRequired,
};

export default Card