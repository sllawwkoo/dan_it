import { Component } from "react";
import Card from "../Card";
import PropTypes from 'prop-types';
import "./style.scss"

class CardList extends Component {
	render() {
		const { cards, toggleModal, isModal, addToBasket, addToFavorite, selectedCard, favsCards, basketCards } = this.props;
		return (
			<div className="cards__body">
				{cards.map(card =>
					<Card key={card.article}
						card={card}
						toggleModal={() => toggleModal(card.article)}
						isFavorite={favsCards && favsCards.some(item => item.article === card.article)}
						isBasket={basketCards.some(item => item.article === card.article)}
						isModal={isModal && selectedCard === card.article}
						addToBasket={() => addToBasket(card)}
						addToFavorite={() => addToFavorite(card)}
					/>
				)}
			</div>
		)
	}
};

CardList.propTypes = {
	cards: PropTypes.arrayOf(PropTypes.shape({
		article: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		color: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		url: PropTypes.string.isRequired
	})).isRequired,
	toggleModal: PropTypes.func.isRequired,
	isModal: PropTypes.bool.isRequired,
	addToBasket: PropTypes.func.isRequired,
	addToFavorite: PropTypes.func.isRequired,
	selectedCard: PropTypes.string.isRequired,
	favsCards: PropTypes.arrayOf(PropTypes.shape({
		article: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		color: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		url: PropTypes.string.isRequired
	})).isRequired,
	basketCards: PropTypes.arrayOf(PropTypes.shape({
		article: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		color: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		url: PropTypes.string.isRequired
	})).isRequired
};

export default CardList