import { Component } from 'react';
import Header from './components/Header';
import CardList from './components/CardList';
import PropTypes from "prop-types";
import './App.scss';
import { getDataFromLS } from './helper/getDataFromLS';

class App extends Component {
	constructor(props) {
		super(props)

		this.state = {
			cards: [],
			error: null,
			isModal: false,
			basketCards: [],
			favsCards: [],
			selectedCard: ''
		}
	}

	componentDidMount() {
		const basketCardsFromLS = getDataFromLS('basketCards');
		const favsCardsFromLS = getDataFromLS('favsCards');

		this.setState({ basketCards: basketCardsFromLS, favsCards: favsCardsFromLS })

		fetch('/data.json')
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						cards: result
					});
				},
				(error) => {
					this.setState({
						error: error
					});
				}
			);
	}

	componentDidUpdate(prevState) {
		if (prevState.favsCards !== this.state.favsCards) {
			localStorage.setItem('favsCards', JSON.stringify(this.state.favsCards));
		}
		if (prevState.basketCards !== this.state.basketCards) {
			localStorage.setItem('basketCards', JSON.stringify(this.state.basketCards));
		}
	}

	toggleModal = (article) => {
		if (this.state.selectedCard === article) {
			this.setState(prevState => {
				return {
					...prevState,
					isModal: false,
					selectedCard: ''
				}
			})
		} else {
			this.setState(prevState => {
				return {
					...prevState,
					isModal: true,
					selectedCard: article
				}
			})
		}
	}

	addToBasket = (card) => {
		const index = this.state.basketCards.findIndex(item => item.article === card.article);

		if (index === -1) {
			const newCardsBasket = [...this.state.basketCards, card];
			this.setState({ basketCards: newCardsBasket });
			this.toggleModal();
		}
	}

	addToFavorite = (card) => {
		if (card && card.article) {
			const index = this.state.favsCards.findIndex(item => item.article === card.article);

			if (index !== -1) {
				const newFavsCards = this.state.favsCards.filter(item => item.article !== card.article);
				this.setState({ favsCards: newFavsCards });
			} else {
				const newFavsCards = [...this.state.favsCards, card];
				this.setState({ favsCards: newFavsCards });
			}
		}
	}

	render() {
		const { error, cards, isFavorite, isModal, selectedCard, favsCards, basketCards } = this.state;
		return (
			<div className='wrapper'>
				<Header countBasket={this.state.basketCards.length} countFave={this.state.favsCards.length} />
				<main className='page'>
					{error ? <div>Error: {error.message}</div>
						: <section className='page__cards cards'>
							<div className="cards__container container">
								<h1 className='cards__title'>Мобільні телефони <span>POCO</span> </h1>
								<>
									<CardList cards={cards}
										isFavorite={isFavorite}
										isModal={isModal}
										selectedCard={selectedCard}
										favsCards={favsCards}
										basketCards={basketCards}
										toggleModal={this.toggleModal}
										addToBasket={this.addToBasket}
										addToFavorite={this.addToFavorite}
									/>
								</>
							</div>
						</section>
					}
				</main>
			</div>
		)
	}
}

App.propTypes = {
	countBasket: PropTypes.number,
	countFave: PropTypes.number,
};

export default App;

