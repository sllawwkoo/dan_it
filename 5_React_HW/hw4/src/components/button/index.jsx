import "./style.scss";

export function Button(props) {
	const { backgroundColor, text, className, onClick } = props;		

	return (
		<button className={className} style={{backgroundColor: backgroundColor}} onClick={onClick}>
			{text}
		</button>
	)
}