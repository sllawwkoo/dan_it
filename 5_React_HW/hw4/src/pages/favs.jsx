import { ProductList } from "../components/product-list";

export function Favs(props) {
	const { toggleModal, addToBasket, addToFavorite, selectedCard } = props;

	return (
		<>
			<h1 style={{ textAlign: 'center' }}>Улюблені товари</h1>
			<div className="cards__body">
				<ProductList
					isFavorite={true}
					toggleModal={toggleModal}
					addToBasket={addToBasket}
					addToFavorite={addToFavorite}
					selectedCard={selectedCard}
				/>
			</div>
		</>
	);
}