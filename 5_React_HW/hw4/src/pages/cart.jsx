import { ProductList } from "../components/product-list";
import { useSelector } from "react-redux";

export function Cart(props) {

	const { addToFavorite, removeFromBasket, toggleModal, selectedCard } = props;
	const { basketCards } = useSelector((state) => state.basketCards);

	return (
		<>
			<h1 style={{ textAlign: 'center' }}>Кошик</h1>
			<div className="cards__body">
				{basketCards.length === 0 ? (
					<p>Ваш кошик порожній</p>
				) : (
					<ProductList
						addToFavorite={addToFavorite}
						removeFromBasket={removeFromBasket}
						isCart={true}
						toggleModal={toggleModal}
						selectedCard={selectedCard}
					/>
				)
				}
			</div>
		</>
	);
}