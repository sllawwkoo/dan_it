import { ProductList } from "../components/product-list";
import { Form } from "../components/form";
import { useSelector } from "react-redux";

export function Cart(props) {

	const { addToFavorite, removeFromBasket, toggleModal, selectedCard } = props;
	const { basketCards } = useSelector((state) => state.basketCards);

	return (
		<>
			<h1 className="cards__title" >Кошик</h1>
			<div className="cards__body">
				{basketCards.length === 0 ? (
					<p>Ваш кошик порожній</p>
				) : (
					<ProductList
						addToFavorite={addToFavorite}
						removeFromBasket={removeFromBasket}
						isCart={true}
						toggleModal={toggleModal}
						selectedCard={selectedCard}
					/>
				)
				}
			</div>
			{basketCards.length === 0 ? null : <Form basketCards={basketCards} /> }
		</>
	);
}