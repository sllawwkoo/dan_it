// Core
import { combineReducers } from "redux";

// Reducers

import { isModalReducer as isModal } from "./reducers/isModal";
import { cardsReducer as cards } from "./reducers/cards";
import { basketCardsReducer as basketCards } from "./reducers/basketCards";
import { favsCardsReducer as favsCards } from "./reducers/favsCards";

export const rootReducer = combineReducers({ isModal, cards, basketCards, favsCards });
