export const isModalTypes = {
	IS_MODAL: "IS_MODAL",
};

export const cardsTypes = {
	FETCH_DATA_START: "FETCH_DATA_START",
	FETCH_DATA_SUCCESS: "FETCH_DATA_SUCCESS",
	FETCH_DATA_ERROR: "FETCH_DATA_ERROR",
};

export const basketCardsTypes = {
	SET_BASKET_CARDS: "SET_BASKET_CARDS",
};

export const favsCardsTypes = {
	SET_FAVS_CARDS: "SET_FAVS_CARDS",
}

