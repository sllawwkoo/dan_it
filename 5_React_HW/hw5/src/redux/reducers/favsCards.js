import { favsCardsTypes } from "../types";
import { getDataFromLS } from "../../helper";

const initialState = {
	favsCards: getDataFromLS("cardsFave"),
}

export function favsCardsReducer(state = initialState, action) {
	switch (action.type) {
		case favsCardsTypes.SET_FAVS_CARDS:
			return {
				...state,
				favsCards: action.payload,
			}
		default:
			return state
	}
}