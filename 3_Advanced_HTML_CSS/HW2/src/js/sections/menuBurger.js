const iconMenu = document.querySelector('.menu__icon');
const menuBody = document.querySelector('.menu__list');
if (iconMenu) {
	iconMenu.addEventListener("click", () => {
		iconMenu.classList.toggle('active');
		menuBody.classList.toggle('active');
	});
}