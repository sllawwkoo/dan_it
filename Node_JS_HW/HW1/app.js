const http = require('http');
const url = require('url');

let requestCount = 0;

const server = http.createServer((req, res) => {
  const parsedUrl = url.parse(req.url, true);
  if (parsedUrl.pathname === '/') {
    requestCount += 1;
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({
      message: 'Request handled successfully',
      requestCount,
    }));
  } else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Not Found');
  }
});

let PORT = 3000;
const args = process.argv.slice(2);
const portIndex = args.findIndex(arg => arg.startsWith('--port='));
if (portIndex !== -1) {
  const portStr = args[portIndex].split('=')[1];
  PORT = parseInt(portStr, 10);
}

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
