import { Card, CurrencyEnum, Transaction } from "./index";
// import crypto from "crypto";
import { v4 as uuidv4 } from "uuid";

describe("Card", () => {
	let card: Card;

	beforeEach(() => {
		card = new Card();
	});

	it("should add transactions and get correct balance", () => {
		// Додавання транзакцій
		const transactionId1 = card.addTransaction(
			new Transaction(100, CurrencyEnum.USD)
		);
		const transactionId2 = card.addTransaction(CurrencyEnum.UAH, 200);

		// Отримання транзакції за Id
		const transaction1 = card.getTransaction(transactionId1);
		expect(transaction1).toEqual(
			expect.objectContaining({ amount: 100, currency: CurrencyEnum.USD })
		);

		const transaction2 = card.getTransaction(transactionId2);
		expect(transaction2).toEqual(
			expect.objectContaining({ amount: 200, currency: CurrencyEnum.UAH })
		);

		// Отримання балансу за валютою
		const usdBalance = card.getBalance(CurrencyEnum.USD);
		expect(usdBalance).toBe(100);

		const uahBalance = card.getBalance(CurrencyEnum.UAH);
		expect(uahBalance).toBe(200);
	});
});
