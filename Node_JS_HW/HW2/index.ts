// import crypto from "crypto";
import { v4 as uuidv4 } from "uuid";

// Enum для валют
export enum CurrencyEnum {
	USD = "USD",
	UAH = "UAH",
}

// Клас транзакції
export class Transaction {
	id: string;

	constructor(public amount: number, public currency: CurrencyEnum) {
		// this.id = crypto.randomUUID();
		this.id = uuidv4();
	}
}

// Клас картки
export class Card {
	private transactions: Transaction[] = [];

	// Метод додавання транзакції + перевантаження
	public addTransaction(transaction: Transaction): string;
	public addTransaction(currency: CurrencyEnum, amount: number): string;
	public addTransaction(
		arg1: Transaction | CurrencyEnum,
		arg2?: number
	): string {
		let transaction: Transaction;
		if (arg1 instanceof Transaction) {
			transaction = arg1;
		} else {
			transaction = new Transaction(arg2!, arg1);
		}
		this.transactions.push(transaction);
		return transaction.id;
	}

	// Метод отримання транзакції за Id
	public getTransaction(id: string): Transaction | undefined {
		return this.transactions.find((transaction) => transaction.id === id);
	}

	// Метод отримання балансу за вказаною валютою
	public getBalance(currency: CurrencyEnum): number {
		return this.transactions
			.filter((transaction) => transaction.currency === currency)
			.reduce((total, transaction) => total + transaction.amount, 0);
	}
}

const card = new Card();

// Додавання транзакцій
const transactionId1 = card.addTransaction(
	new Transaction(100, CurrencyEnum.USD)
);
const transactionId2 = card.addTransaction(CurrencyEnum.UAH, 200);

// Отримання транзакції за Id
const transaction1 = card.getTransaction(transactionId1);
console.log("Transaction 1:", transaction1);
const transaction2 = card.getTransaction(transactionId2);
console.log("Transaction 2:", transaction2);

// Отримання балансу за валютою
const usdBalance = card.getBalance(CurrencyEnum.USD);
const uahBalance = card.getBalance(CurrencyEnum.UAH);

console.log("Balance in USD:", usdBalance);
console.log("Balance in UAH:", uahBalance);

// Результат роботи отримати в консолі = ts-node index.ts
// Перевірити за допомогою тестування = npm run test