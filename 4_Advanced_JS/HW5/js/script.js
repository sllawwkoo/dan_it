/* Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
Технічні вимоги:
При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts
Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
Необов'язкове завдання підвищеної складності
Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
*/
let users = [];
let posts = [];
const root = document.querySelector('#root');
const addPost = document.querySelector('.btn__add');

class Card {
	constructor(post) {
		this.post = post;
		this.element = this.createCard();
	}

	createCard() {
		const { title, body, userId } = this.post;
		const user = users.find((user) => user.id === userId);

		const card = document.createElement('div');
		card.classList.add("card", `card${userId}`);
		card.insertAdjacentHTML('beforeend', `
			<div class="card__header">
				<div class="card__info">
					<h3 class="card__name">${user.name}</h3>
					<div class="card__username">@${user.username}</div>
					<div class="card__email">${user.email}</div>
				</div>
			</div>	
			<div class="card__block">
				<h3 class="card__title">${title}</h3>
				<p class="card__body">${body}</p>
		   </div>`);
		const editCard = document.createElement('button');
		editCard.classList.add("btn", "btn__edit");
		editCard.textContent = 'Edit';

		const deleteCard = document.createElement('button');
		deleteCard.classList.add("btn", "btn__del");
		deleteCard.textContent = 'Delete'

		card.append(editCard, deleteCard)

		deleteCard.addEventListener("click", this.deletePost.bind(this))
		editCard.addEventListener("click", this.editPost.bind(this))

		return card
	}

	async deletePost() {
		try {
			await fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
				method: "DELETE",
			})
			this.element.remove();
		} catch (error) {
			console.log(error)
		}
	}

	async editPost() {
		const { title, body } = this.post;
		const newTitle = prompt('Enter new title:', title);
		const newBody = prompt('Enter new body:', body);
		if (newTitle !== null && newBody !== null) {
			this.post.title = newTitle;
			this.post.body = newBody;
			try {
				await fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
					},
					body: JSON.stringify(this.post),
				});
				this.element.querySelector('.card__title').textContent = newTitle;
				this.element.querySelector('.card__body').textContent = newBody;
			} catch (error) {
				console.log(error)
			}
		}
	}
}

async function getData() {
	root.insertAdjacentHTML('beforeend', `
	   <div class="loader"></div>`)
	const [usersResponse, postsResponse] = await Promise.all([
		await fetch('https://ajax.test-danit.com/api/json/users', {
			method: 'GET'
		}),
		await fetch('https://ajax.test-danit.com/api/json/posts', {
			method: 'GET'
		})])
	users = await usersResponse.json();
	posts = await postsResponse.json();

	document.querySelector('.loader').remove()
	posts.forEach(post => {
		const card = new Card(post);
		root.append(card.element)
	});

}

getData()

function createModalWindow() {
	const modal = document.createElement('div');
	modal.classList.add('modal');
	modal.innerHTML = `
  <div class="modal__content">
    <form>
      <label for="post-title">Title:</label>
      <input type="text" id="post-title" required>

      <label for="post-body">Body:</label>
      <textarea id="post-body" required></textarea>

      <button type="submit" id="submit-btn">Submit</button>
      <button type="button" id="cancel-btn">Cancel</button>
    </form>
  </div>
`
	return modal
};

addPost.addEventListener('click', () => {
	modal = createModalWindow()
	root.append(modal);

	const cancelBtn = modal.querySelector('#cancel-btn');
	cancelBtn.addEventListener('click', () => {
		modal.remove();
	});

	const form = modal.querySelector('form');
	form.addEventListener('submit', async (e) => {
		e.preventDefault();

		const newPost = {
			title: form.querySelector('#post-title').value,
			body: form.querySelector('#post-body').value,
			userId: 1,
		};

		try {
			const response = await fetch('https://ajax.test-danit.com/api/json/posts', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(newPost),
			});
			const data = await response.json();

			const card = new Card(data);
			root.prepend(card.element);
			modal.remove();
		} catch (error) {
			console.log(error);
		}
	});
});
