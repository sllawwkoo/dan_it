// Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.


/* 1. Завантаження файлу -  try...catch може допомогти уникнути краху програми, якщо файл не вдалося завантажити з-за помилки читання або відсутності файлу. 
	2. При взаємодії з базою даних можуть виникати помилки, такі як втрата з'єднання з базою даних або невірно сформований запит. Використання конструкції try...catch може допомогти уникнути краху програми та надати користувачеві відповідну повідомлення про помилку.
	3. Якщо ми працюємо з даними у форматі JSON, то конструкція try...catch може бути використана для перевірки на правильність структури даних та обробки винятків, якщо дані не можуть бути розібрані через неправильний формат.
	4. Робота зі вводом/виводом - якщо програма взаємодіє зі зовнішніми джерелами даних через ввід/вивід (наприклад, читання/запис даних у файл або робота з консоллю), то використання конструкції try...catch може допомогти обробити помилки, які можуть виникнути при таких операціях (наприклад, помилки читання файлу або введення неправильного формату даних).
	Взагалі, конструкція try...catch допомагає програмі бути більш стійкою до помилок та винятків, щодопомагає підвищити її якість та зручність для користувача
*/

/* Завдання
Дано масив books.
Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогоюJavascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
*/
const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

const base = ['author', 'name', 'price'];
const root = document.querySelector('#root');

function checkAndOotputList(arr, param, element = document.body) {
	const ul = document.createElement('ul');
	element.append(ul);

	arr.forEach((obj, index) => {
		try {
			const emptyProp = param.filter(prop => !Object.keys(obj).includes(prop));
			if (emptyProp.length > 0) {
				throw new Error(`В об"єкті № ${index + 1} відсутні властивості: ${emptyProp.join(', ')}`)
			}
			createElementList(obj, ul)
		} catch (error) {
			console.log(`${error.name}: ${error.message}`);
		}
	});
}

function createElementList(obj, element) {
	Object.entries(obj).map(([key, value]) => element.insertAdjacentHTML('beforeend', `<li>${key}: ${value}</li>`));
	element.insertAdjacentHTML('beforeend', `<br>`)
}

checkAndOotputList(books, base, root)

