// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX - це технологія, що дозволяє відправляти запити на сервер та отримувати відповіді без перезавантаження сторінки.

/* Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films 
та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

Необов'язкове завдання підвищеної складності
Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. 
Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
*/

const baseProperties = ['episodeId', 'name', 'openingCrawl'];
const root = document.querySelector('#root');
const char = 'characters';

function getData() {
	fetch('https://ajax.test-danit.com/api/swapi/films')
		.then(response => response.json())
		.then(data => renderData(data))
		.catch(error => console.error('Something went wrong', error))
}

function getDeepArray(array) {
	const deepArr = array.map(item =>
		fetch(item)
			.then(response => response.json())
			.catch(error => console.error('Something went wrong', error))
	);
	return Promise.all(deepArr)
}

function createElementList(obj, baseArr) {
	let htmlProp = '';
	baseArr.forEach(key => {
		if (obj.hasOwnProperty(key)) {
			htmlProp += `<p class="${key}"><strong>${key}</strong>: ${obj[key]}</p>`;
		}
	}); return htmlProp
}

function renderData(arr) {
	arr.forEach((obj, index) => {
		root.insertAdjacentHTML('beforeend', `
		<div class="container" data-id=${index}>
			${createElementList(obj, baseProperties)}
			<div class="loader"></div>
			<h4>${char}</h4>
			<ul class="list${index}"></ul>		
		</div>`);
		getDeepArray(obj[char])
			.then(data => {
				document.querySelector('.loader').remove()
				data.forEach(item => {
					document.querySelector(`.list${index}`).insertAdjacentHTML('beforeend', `<li>${item['name']}</li>`)
				})
			}).catch(error => {
				console.error('Promis.all error', error)
			})
	});
}

getData();

