/* Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

У Javascript асинхронність означає, що певні дії, які зазвичай виконуються послідовно, можуть виконуватися паралельно.
Наприклад, коли ми викликаємо функцію, яка займає деякий час на обробку, замість того, щоб чекати, поки функція завершить 
свою роботу, ми можемо продовжувати виконувати інші дії, за допомогою асинхроного коду, використовуючи проміси
та async/await.
*/

/*
Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.
*/


async function getLocation() {
	let response = await fetch('https://api.ipify.org/?format=json');
	let ip = await response.json();

	let response2 = await fetch(`http://ip-api.com/json/${ip.ip}?fields=message,continent,country,regionName,city,district`);
	let location = await response2.json();
	console.log(location);
	createElementList(location, document.querySelector('#root'))
}

function createElementList(obj, element = document.body) {
	Object.entries(obj).map(([key, value]) => element.insertAdjacentHTML('beforeend', `<div><strong>${key}</strong>: ${value}</div>`));
}

document.querySelector('.btn').addEventListener('click', getLocation)