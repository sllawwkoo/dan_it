// 1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Прототипне наслідування в JavaScript - це механізм, який дозволяє створювати об'єкти на основі інших об'єктів, 
// що називаються прототипами.У JavaScript всі об'єкти мають властивість prototype, яка вказує на інший об'єкт, 
// який використовується як прототип. Об'єкт-прототип містить методи та властивості, які можуть бути успадковані дочірніми об'єктами.
// Прототипне наслідування дозволяє створювати код більш ефективним і гнучким, оскільки можна використовувати вже існуючий код замість 
// повторного написання його в кожному об'єкті.
// _______________________________________________________________________________________

// 2.Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Виклик super() у конструкторі класу-нащадка використовується для виклику конструктора класу-батька. 
// Це необхідно для того, щоб унаслідувані властивості і методи були ініціалізовані правильно.
// __________________________________________________________________________________________

class Employee {
	constructor(name, age, salary) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}

	get name() {
		return this._name;
	}

	set name(newName) {
		this._name = newName;
	}

	get age() {
		return this._age;
	}

	set age(newAge) {
		this._age = newAge;
	}

	get salary() {
		return this._salary;
	}

	set salary(newSalary) {
		this._salary = newSalary;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this._lang = lang;
	}

	get lang() {
		return this._lang;
	}

	set lang(newLang) {
		this._lang = newLang;
	}

	get salary() {
		return super.salary * 3;
	}
}

const user1 = new Programmer('Ivan', 26, 1000, 'English');
const user2 = new Programmer('Petro', 32, 2000, 'Spanish');
const user3 = new Programmer('Vasil', 40, 3000, "Germany");

